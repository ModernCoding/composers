<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComposersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('composers')->insert(['fullname' => 'Maurice Ravel', 'country_id' => 63]);
      DB::table('composers')->insert(['fullname' => 'Ludwig van Beethoven', 'country_id' => 68]);
      DB::table('composers')->insert(['fullname' => 'Wolfgang Amadeus Mozart', 'country_id' => 68]);
      DB::table('composers')->insert(['fullname' => 'Giuseppe Verdi', 'country_id' => 87]);
      DB::table('composers')->insert(['fullname' => 'Anton Bruckner', 'country_id' => 11]);
      DB::table('composers')->insert(['fullname' => 'Johann Sebastian Bach', 'country_id' => 68]);
      DB::table('composers')->insert(['fullname' => 'Antonio Vivaldi', 'country_id' => 87]);
      DB::table('composers')->insert(['fullname' => 'Antonín Dvořák', 'country_id' => 49]);
    }
}
