<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterpiecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('masterpieces')->insert(['name' => 'Le Quattro Stagioni', 'url' => 'https://www.youtube.com/embed/czlsy6pnX8s', 'composer_id' => 7]);
      DB::table('masterpieces')->insert(['name' => 'Nabucco', 'url' => 'https://www.youtube.com/embed/1iX4HPVW-wI', 'composer_id' => 4]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°5 in C minor', 'url' => 'https://www.youtube.com/embed/lNtb-ly1I_k', 'composer_id' => 2]);
      DB::table('masterpieces')->insert(['name' => 'Boléro', 'url' => 'https://www.youtube.com/embed/Az8P_QVzurs', 'composer_id' => 1]);
      DB::table('masterpieces')->insert(['name' => 'Les Vêpres Siciliennes', 'url' => 'https://www.youtube.com/embed/D76FzS5h4n8', 'composer_id' => 4]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°9 in D minor', 'url' => 'https://www.youtube.com/embed/7pszB5Ic2KA', 'composer_id' => 2]);
      DB::table('masterpieces')->insert(['name' => 'Aida', 'url' => 'https://www.youtube.com/embed/5eHoV0pLlHw', 'composer_id' => 4]);
      DB::table('masterpieces')->insert(['name' => 'Ma Mère l’Oye', 'url' => 'https://www.youtube.com/embed/ZFVu8TP77Tw', 'composer_id' => 1]);
      DB::table('masterpieces')->insert(['name' => 'Eine Kleine Nachtmusik', 'url' => 'https://www.youtube.com/embed/o1FSN8_pp_o', 'composer_id' => 3]);
      DB::table('masterpieces')->insert(['name' => 'Die Zauberflöte', 'url' => 'https://www.youtube.com/embed/ZNEOl4bcfkc', 'composer_id' => 3]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°25 in G minor', 'url' => 'https://www.youtube.com/embed/7lC1lRz5Z_s', 'composer_id' => 3]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°40 in G minor', 'url' => 'https://www.youtube.com/embed/p8bZ7vm4_6M', 'composer_id' => 3]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°41 in C major', 'url' => 'https://www.youtube.com/embed/p6pQC3OqDW4', 'composer_id' => 3]);
      DB::table('masterpieces')->insert(['name' => 'La Traviata', 'url' => 'https://www.youtube.com/embed/Ixbrot9hq5U', 'composer_id' => 4]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°4', 'url' => 'https://www.youtube.com/embed/LY7m119eOys', 'composer_id' => 5]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°9', 'url' => 'https://www.youtube.com/embed/UCnvt_tg88Y', 'composer_id' => 5]);
      DB::table('masterpieces')->insert(['name' => 'Matthäus-Passion', 'url' => 'https://www.youtube.com/embed/Xdl0m1v5el8', 'composer_id' => 6]);
      DB::table('masterpieces')->insert(['name' => 'Suite BWV 1067', 'url' => 'https://www.youtube.com/embed/tYzWzZZ2bW8', 'composer_id' => 6]);
      DB::table('masterpieces')->insert(['name' => 'Brandenburgisches Konzert Nr. 3', 'url' => 'https://www.youtube.com/embed/mB1M2HaEbI4', 'composer_id' => 6]);
      DB::table('masterpieces')->insert(['name' => 'Gloria RV589', 'url' => 'https://www.youtube.com/embed/RMHguvZPcqQ', 'composer_id' => 7]);
      DB::table('masterpieces')->insert(['name' => 'Symphony n°9 "From the New World"', 'url' => 'https://www.youtube.com/embed/zkpc0DiXlI0', 'composer_id' => 8]);
      DB::table('masterpieces')->insert(['name' => 'Slavonic Dances', 'url' => 'https://www.youtube.com/embed/lQ-AOgFujNQ', 'composer_id' => 8]);
  }
}
