<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterpiecesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('masterpieces', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('composer_id');
      $table->string('name');
      $table->string('url');
      $table->timestamps();

      $table
        ->foreign('composer_id')
        ->references('id')
        ->on('composers');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('masterpieces');
  }
}
