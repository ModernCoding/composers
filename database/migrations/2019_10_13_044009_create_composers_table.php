<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComposersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('composers', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('fullname');
      $table->unsignedBigInteger('country_id');
      $table->timestamps();

      $table
        ->foreign('country_id')
        ->references('id')
        ->on('countries');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('composers');
  }
}
