<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConductorsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('conductors', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('fullname');
      $table->unsignedBigInteger('country_id');
      $table->unsignedBigInteger('composer_id');
      $table->timestamps();

      $table
        ->foreign('country_id')
        ->references('id')
        ->on('countries');

      $table
        ->foreign('composer_id')
        ->references('id')
        ->on('composers');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('conductors');
  }
}
