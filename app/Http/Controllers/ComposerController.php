<?php

namespace App\Http\Controllers;

use App\Composer;
use App\Country;
use App\Masterpiece;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;


class ComposerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $composers = Composer::with('country')->orderBy('fullname')->get();
    // DB::enableQueryLog();
    // dd(DB::getQueryLog(Composer::orderBy('fullname')->get()));
    return view('composer/index', compact('composers'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $composer = new Composer;
    $countries = Country::orderBy('label')->get();

    return view(
      'composer/create',
      compact('composer', 'countries')
    );
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $composer = new Composer;

    $validator = Validator::make(
      $request->all(),
      $composer->rulesCreate(),
      $composer->messages
    );

    if ($validator->fails()) {
      return redirect()
          ->route('composers.create')
          ->withErrors($validator)
          ->withInput();
    }

    Composer::create($request->all());
    
    return redirect()
        ->route('composers.index')
        ->with('success','Add success!');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Composer  $composer
   * @return \Illuminate\Http\Response
   */
  public function show(Composer $composer)
  {
    $composer = DB::table('composers')

        ->join(
          'countries',
          'composers.country_id',
          '=',
          'countries.id'
        )

        ->select(
          'composers.id',
          'composers.fullname',
          'countries.label as country_label'
        )

        ->where('composers.id', $composer->id)
        ->first();


    $masterpieces = Masterpiece::where('composer_id', $composer->id)
        ->orderBy('name')
        ->get();


    return view(
      'composer/show',
      compact('composer', 'masterpieces')
    );
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Composer  $composer
   * @return \Illuminate\Http\Response
   */
  public function edit(Composer $composer)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Composer  $composer
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Composer $composer)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Composer  $composer
   * @return \Illuminate\Http\Response
   */
  public function destroy(Composer $composer)
  {
    //
  }
}
