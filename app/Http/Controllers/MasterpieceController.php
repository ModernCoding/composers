<?php

namespace App\Http\Controllers;

use App\Masterpiece;
use App\Composer;

use Illuminate\Http\Request;
use Validator;


class MasterpieceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $masterpiece = new Masterpiece;
    $composers = Composer::orderBy('fullname')->get();

    return view(
      'masterpiece/create',
      compact('masterpiece', 'composers')
    );
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $masterpiece = new Masterpiece;

    $myRequest = $request->all();
    
    $myRequest["url"] = str_replace(
        'watch?v=',
        'embed/',
        $myRequest["url"]
      );
    
    $request->replace($myRequest);

    $validator = Validator::make(
      $request->all(),
      $masterpiece->rules(),
      $masterpiece->messages
    );

    if ($validator->fails()) {
      return redirect()
          ->route('masterpieces.create')
          ->withErrors($validator)
          ->withInput();
    }

    Masterpiece::create($request->all());
    
    return redirect()
        ->route('composers.index')
        ->with('success','Add success!');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Masterpiece  $masterpiece
   * @return \Illuminate\Http\Response
   */
  public function show(Masterpiece $masterpiece)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Masterpiece  $masterpiece
   * @return \Illuminate\Http\Response
   */
  public function edit(Masterpiece $masterpiece)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Masterpiece  $masterpiece
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Masterpiece $masterpiece)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Masterpiece  $masterpiece
   * @return \Illuminate\Http\Response
   */
  public function destroy(Masterpiece $masterpiece)
  {
    //
  }
}
