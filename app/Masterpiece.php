<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masterpiece extends Model
{
  // fields in the table
  protected $guarded = ['id'];

  // fields in the table
  protected $fillable = ['composer_id', 'name', 'url'];
  
  // set timestamp, allow to use
  public $timestamps=true;


  public $messages = [
    'name.required'   =>  'Please enter the name of the masterpiece',
    'url.required'    =>  'Please enter the URL'
  ];


  public function rules()
  {
    return [
      'name'    =>  'required',
      'url'     =>  'required'
    ];
  }


  public function composer() {
    return $this->belongsTo('App\Composer');
  }
}
