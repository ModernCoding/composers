<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Composer extends Model
{
  // fields in the table
  protected $guarded = ['id'];

  // fields in the table
  protected $fillable = ['country_id', 'fullname'];
  
  // set timestamp, allow to use
  public $timestamps=true;


  public $messages = [
    'fullname.required'   =>  'Please enter the name of the composer',
    'fullname.unique'     =>  'This composer already exists'
  ];


  public function rulesCreate()
  {
    return [
      'fullname'   =>  'required|unique:composers,fullname'
    ];
  }


  public function rulesUpdate($id)
  {
    return [
      'fullname'   =>  'required|unique:composers,fullname,'.$id
    ];
  }


  public function country() {
    return $this->belongsTo('App\Country');
  }

  public function masterpieces() {
    return $this->hasMany('App\Masterpiece');
  }

}
