import { Controller } from "stimulus";

export default class extends Controller {

  stopAll(e) {

    let $this = $(e.target);
    $this.addClass('do-not-stop media-is-playing');

    $.each(
      $('iframe.media-is-playing').not('.do-not-stop'),

      (index, iframe) => {
        $(iframe)
          .attr('src', $(iframe).attr('src'))
          .removeClass('media-is-playing');
      }
    );

    $this.removeClass('do-not-stop');

  }

}