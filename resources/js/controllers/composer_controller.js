import { Controller } from "stimulus";
import { resize } from "../resizing";


export default class extends Controller {

  connect() {
    console.log(`Hello!`);
  }


  show(e) {

    let $composer = $(e.target).closest('.my-composer');

    fetch(this.data.get("url"))
      .then(response => response.text())
      
      .then(html => {
        $('.my-composer').removeClass('d-none');
        $composer.addClass('d-none');
        
        $("#my-composer-detail")
          .removeClass('d-none')
          .html(html);
        
        $('html, body').animate({ scrollTop: 0 }, 600);
        resize();
      });
  
  }


  hide() {
    $("#my-composer-detail").addClass('d-none').html(null);
    $('.my-composer').removeClass('d-none');
    $('html, body').animate({ scrollTop: 0 }, 600);
    resize();
  }

}