@if($errors->any())
  <div class="alert alert-danger">
    @foreach ($errors->all() as $error)
      <div>{!! $error !!}</div>
    @endforeach
  </div>
@endif


<form
  data-controller="form"
  data-action="submit->form#submit"
  data-form-action="{!! $action !!}"
  data-form-method="post"
  data-form-redirect="{!! $redirect !!}"
  enctype="multipart/form-data"
>
  @csrf

  @if($masterpiece['id'] != NULL)
    @method('PATCH')
    <input type="hidden" name="id" value="{!! $masterpiece['id'] !!}">
  @endif 

  <div class="row my-padding-bottom-19">
    <div class="col-md-3 col-lg-4 my-padding-bottom-8">
      <label for="name">Name:<label>
    </div>

    <div class="col-md-9 col-lg-8 my-padding-bottom-8">
      <input
        id="name"
        type="text"
        class="form-control"
        name="name"
        
        value="{!! 
                old (
                  'name',
                  isset($masterpiece) ? $masterpiece['name'] : NULL
                )
          !!}"
      >
    </div>
  </div>


  <div class="row my-padding-bottom-19">
    <div class="col-md-3 col-lg-4 my-padding-bottom-8">
      <label for="composer-id">Composer:<label>
    </div>

    <div class="col-md-9 col-lg-8 my-padding-bottom-8">
      <select name="composer_id" class="form-control" id="composer-id">

        @foreach ($composers as $composer)
          <option
            value="{!! $composer['id'] !!}"
            {!!
                old (
                  'composer_id',
                  isset($masterpiece) && $masterpiece['composer_id'] ==
                    $composer['id'] ? 'selected' : NULL 
                )
            !!}
          >
            {!! $composer['fullname'] !!}
          </option>
        @endforeach

      </select>
    </div>
  </div>


  <div class="row my-padding-bottom-19">
    <div class="col-md-3 col-lg-4 my-padding-bottom-8">
      <label for="url">URL:<label>
    </div>

    <div class="col-md-9 col-lg-8 my-padding-bottom-8">
      <input
        id="url"
        type="text"
        class="form-control"
        name="url"
        
        value="{!! 
                old (
                  'url',
                  isset($masterpiece) ? $masterpiece['url'] : NULL
                )
          !!}"
      >
    </div>
  </div>


  <!-- button Save -->

  <div class="row">
    <div class="col-md-3 col-lg-4"></div>

    <div class="col-md-9 col-lg-8">
      <button
        type="submit"
        class="btn btn-sm btn-success my-margin-bottom-8"
      >
        <i class="fas fa-check-circle"></i>
        <span>Save</span>
      </button>
    </div>
  </div>

</form>