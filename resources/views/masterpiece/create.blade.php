@extends('_layouts.app')

@section('header')

  <h1 class="text-center">
    Add a new masterpiece
  </h1>

@endsection


@section('content')

  @include(
    'masterpiece/_form',
    
    [
      'errors'        =>  $errors,
      'action'        =>  URL::action('MasterpieceController@store'),
      'redirect'      =>  URL::action('ComposerController@index'),
      'masterpiece'   =>  $masterpiece
    ]
  )

@endsection