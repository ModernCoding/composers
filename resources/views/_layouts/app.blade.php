<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

  <head>

    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    >

    <title>Composers</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.jpg') }}">
    
    <link
      rel="stylesheet"
      type="text/css"
      href="{{ mix('/css/app.css') }}"
    >

    <script
      defer
      type="text/javascript"
      src="{{ mix('/js/app.js') }}"
    ></script>

  </head>


  <body>

    <section class="container-fluid">
      <div class="row">
        
        <aside
          data-controller="menu"
          data-menu-pathname="/"
          class="col-md-4 col-lg-3 col-xl-2"
        >
          <div
            class="d-flex flex-wrap justify-content-center d-md-block"
          >
            
            <div class="my-aside-button">
              <a
                href="{!! route('home') !!}"
                class="btn btn-sm btn-outline-light"
                data-pathname="/"
              >
                Home
              </a>
            </div>
            
            <div class="my-aside-button">
              <a
                href="{!! route('countries.index') !!}"
                class="btn btn-sm btn-outline-light"
                data-pathname="/countries"
              >
                Countries
              </a>
            </div>
            
            <div class="my-aside-button">
              <a
                href="{!! route('composers.index') !!}"
                class="btn btn-sm btn-outline-light"
                data-pathname="/composers"
              >
                Composers
              </a>
            </div>

          </div>
        </aside>


        <main role="main" class="col">
          <header>
            @yield('header') 
          </header>
          
          <article>
            @yield('content') 
          </article>
        </main>

      </div>
    </section>

  </body>

</html>