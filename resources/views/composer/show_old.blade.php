@extends('_layouts.app')

@section('header')

  <h3 class="text-center">
    {!! $composer->fullname !!}
  </h3>

  <h4 class="text-center">
    {!! $composer->country_label !!}
  </h4>

@endsection


@section('content')

  <div class="row">

    @foreach($masterpieces as $masterpiece)

      <div
        class="
            col-lg-6
            col-xl-4
            text-center
            my-padding-bottom-19
            my-masterpiece
          "
      >
        <div class="bg-danger text-light my-padding-19">
          <div class="my-padding-bottom-12">
            <span class="h4 font-weight-bold">
              {!! $masterpiece["name"] !!}
            </span>
          </div>

          <iframe
            src="{!! $masterpiece["url"] !!}"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>

        </div>
      </div>

    @endforeach

  </div>

@endsection