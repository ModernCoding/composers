@if($errors->any())
  <div class="alert alert-danger">
    @foreach ($errors->all() as $error)
      <div>{!! $error !!}</div>
    @endforeach
  </div>
@endif


<form
  data-controller="form"
  data-action="submit->form#submit"
  data-form-action="{!! $action !!}"
  data-form-method="post"
  data-form-redirect="{!! $redirect !!}"
  enctype="multipart/form-data"
>
  @csrf

  @if($composer['id'] != NULL)
    @method('PATCH')
    <input type="hidden" name="id" value="{!! $composer['id'] !!}">
  @endif 

  <div class="row my-padding-bottom-19">
    <div class="col-md-3 col-lg-4 my-padding-bottom-8">
      <label for="fullname">Fullname:<label>
    </div>

    <div class="col-md-9 col-lg-8 my-padding-bottom-8">
      <input
        id="fullname"
        type="text"
        class="form-control"
        name="fullname"
        
        value="{!! 
                old (
                  'fullname',
                  isset($composer) ? $composer['fullname'] : NULL
                )
          !!}"
      >
    </div>
  </div>


  <div class="row my-padding-bottom-19">
    <div class="col-md-3 col-lg-4 my-padding-bottom-8">
      <label for="country-id">Nationality:<label>
    </div>

    <div class="col-md-9 col-lg-8 my-padding-bottom-8">
      <select name="country_id" class="form-control" id="country-id">

        @foreach ($countries as $country)
          <option
            value="{!! $country['id'] !!}"
            {!!
                old (
                  'country_id',
                  isset($composer) && $composer['country_id'] ==
                    $country['id'] ? 'selected' : NULL 
                )
            !!}
          >
            {!! $country['label'] !!}
          </option>
        @endforeach

      </select>
    </div>
  </div>


  <!-- button Save -->

  <div class="row">
    <div class="col-md-3 col-lg-4"></div>

    <div class="col-md-9 col-lg-8">
      <button
        type="submit"
        class="
            btn
            btn-sm
            btn-success
            my-margin-bottom-8
            my-button-form-submit
          "
      >
        <i class="fas fa-check-circle"></i>
        <span>Save</span>
      </button>
    </div>
  </div>

</form>