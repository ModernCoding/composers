@extends('_layouts.app')

@section('header')

  <h1 class="text-center">
    Composers
  </h1>

  <div
    class="d-flex flex-wrap justify-content-center"
    id="my-composers-action-buttons"
  >
    <a
      href="{!! route('composers.create') !!}"
      class="btn btn-sm btn-outline-success my-margin-4"
    >
      <i class="fas fa-feather-alt my-margin-right-12"></i>
      <span>Add a new composer</span>
    </a>

    <a
      href="{!! route('masterpieces.create') !!}"
      class="btn btn-sm btn-outline-success my-margin-4"
    >
      <i class="fas fa-music my-margin-right-12"></i>
      <span>Add a new masterpiece</span>
    </a>
  </div>

@endsection


@section('content')

  <div
    class="my-margin-bottom-19 my-frame d-none"
    id="my-composer-detail"
  >
  </div>

  <div class="row">

    @foreach($composers as $composer)

      <div class="col-lg-6 col-xl-4 my-padding-bottom-19 my-composer">
        <div class="my-frame">
          <div class="my-padding-bottom-12">
            <span class="h4 font-weight-bold">
              {!! $composer["fullname"] !!}
            </span>
          </div>
          
          <div class="d-flex flex-wrap">

            <div class="my-padding-right-8 my-padding-bottom-8">
              <a
                href="{!! route('composers.show', $composer["id"]) !!}"
                class="btn btn-sm btn-outline-dark"
              >
                <i class="fas fa-eye my-margin-right-12"></i>
                <span>Detail</span>
              </a>
            </div>

            
            <div class="my-padding-right-8 my-padding-bottom-8">
              <a
                href="{!! route('composers.edit', $composer["id"]) !!}" 
                class="btn btn-sm btn-outline-primary"
              >
                <i class="far fa-edit my-margin-right-12"></i>
                <span>Edit</span>
              </a>
            </div>

            <div class="my-padding-bottom-8">
              <button
                class="btn btn-sm btn-danger my-composer-delete"
                data-token="{!! csrf_token() !!}"
                data-url="
                  {!! route('composers.destroy', $composer['id']) !!}
                "
              >
                <i class="far fa-trash-alt my-margin-right-12"></i>
                <span>Delete</span>
              </button>
            </div>

          </div>
        </div>
      </div>

    @endforeach

  </div>

@endsection