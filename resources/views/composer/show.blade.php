@extends('_layouts.none')

@section('content')

  <h3 class="text-center">
    {!! $composer->fullname !!}
  </h3>

  <h4 class="text-center">
    {!! $composer->country_label !!}
  </h4>

  <div class="my-padding-4 text-center">
    <button
      class="btn btn-sm btn-outline-dark"
      data-controller="composer"
      data-action="composer#hide"
    >
      <i class="fas fa-eye-slash my-margin-right-12"></i>
      <span>Hide</span>
    </button>
  </div>


  <div class="row" data-controller="youtube">

    @foreach($masterpieces as $masterpiece)

      <div
        class="
            col-lg-6
            col-xl-4
            text-center
            my-padding-bottom-19
            my-masterpiece
          "
      >
        <div class="bg-danger text-light my-padding-19">
          <div class="my-padding-bottom-12">
            <span class="h4 font-weight-bold">
              {!! $masterpiece["name"] !!}
            </span>
          </div>

          <iframe
            src="{!! $masterpiece["url"] !!}"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
            data-action="mouseenter->youtube#stopAll"
          ></iframe>

        </div>
      </div>

    @endforeach

  </div>

@endsection