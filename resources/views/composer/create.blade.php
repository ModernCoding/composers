@extends('_layouts.app')

@section('header')

  <h1 class="text-center">
    Add a new composer
  </h1>

@endsection


@section('content')

  @include(
    'composer/_form',
    
    [
      'errors'    =>  $errors,
      'action'    =>  URL::action('ComposerController@store'),
      'redirect'  =>  URL::action('ComposerController@index'),
      'composer'  =>  $composer
    ]
  )

@endsection